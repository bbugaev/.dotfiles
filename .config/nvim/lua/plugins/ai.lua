return {
    -- Copilot
    {
        'zbirenbaum/copilot.lua',
        cmd = 'Copilot',
        event = 'InsertEnter',
        config = function()
            require('copilot').setup {
                panel = {
                    enabled = true,
                    auto_refresh = false,
                    keymap = {
                        jump_prev = '<M-p>',
                        jump_next = '<M-n>',
                        accept = '<CR>',
                        refresh = 'gr',
                        open = '<M-i>',
                    },
                    layout = {
                        position = 'bottom', -- | top | left | right
                        ratio = 0.4,
                    },
                },
                suggestion = {
                    enabled = true,
                    auto_trigger = true,
                    debounce = 75,
                    keymap = {
                        accept = '<M-CR>',
                        accept_word = false,
                        accept_line = false,
                        next = '<M-n>',
                        prev = '<M-p>',
                        dismiss = '<C-]>',
                    },
                },
                filetypes = {
                    markdown = true,
                    gitcommit = true,
                },
            }
        end,
    },

    -- Cursor-like editing
    {
        'yetone/avante.nvim',
        event = 'VeryLazy',
        lazy = false,
        version = '*', -- Set this to "*" to always pull the latest release version, or set it to false to update to the latest code changes.
        opts = {
            -- add any opts here
            -- for example
            -- provider = 'openai',
            provider = 'claude',
            openai = {
                endpoint = 'https://api.openai.com/v1',
                model = 'gpt-4o', -- your desired model (or use gpt-4o, etc.)
                timeout = 30000, -- timeout in milliseconds
                temperature = 0, -- adjust if needed
                max_tokens = 4096,
                -- reasoning_effort = "high" -- only supported for reasoning models (o1, etc.)
            },
            claude = {
                endpoint = 'https://api.anthropic.com',
                model = 'claude-3-5-sonnet-20241022',
                temperature = 0,
                max_tokens = 4096,
            },
            windows = {
                width = 40,
            },
            file_selector = {
                provider = 'fzf',
            },
            repo_map = {
                ignore_patterns = {
                    '%.git',
                    '%.worktree',
                    '__pycache__',
                    'node_modules',
                }, -- ignore files matching these
            },
        },
        -- if you want to build from source then do `make BUILD_FROM_SOURCE=true`
        build = 'make',
        -- build = "powershell -ExecutionPolicy Bypass -File Build.ps1 -BuildFromSource false" -- for windows
        dependencies = {
            'nvim-treesitter/nvim-treesitter',
            'stevearc/dressing.nvim',
            'nvim-lua/plenary.nvim',
            'MunifTanjim/nui.nvim',
            --- The below dependencies are optional,
            'hrsh7th/nvim-cmp', -- autocompletion for avante commands and mentions
            'ibhagwan/fzf-lua', -- for file_selector provider fzf
            'nvim-tree/nvim-web-devicons', -- or echasnovski/mini.icons
            'zbirenbaum/copilot.lua', -- for providers='copilot'
            {
                -- support for image pasting
                'HakonHarnes/img-clip.nvim',
                event = 'VeryLazy',
                opts = {
                    -- recommended settings
                    default = {
                        embed_image_as_base64 = false,
                        prompt_for_file_name = false,
                        drag_and_drop = {
                            insert_mode = true,
                        },
                        -- required for Windows users
                        use_absolute_path = true,
                    },
                },
            },
            {
                -- Make sure to set this up properly if you have lazy=true
                'MeanderingProgrammer/render-markdown.nvim',
                opts = {
                    file_types = { 'markdown', 'Avante' },
                },
                ft = { 'markdown', 'Avante' },
            },
        },
    },
}
