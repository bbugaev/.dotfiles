return {
    -- Startup time profiling
    'dstein64/vim-startuptime',

    -- Status line
    {
        'nvim-lualine/lualine.nvim',
        dependencies = { 'nvim-tree/nvim-web-devicons' },
        config = function()
            require('lualine').setup {
                options = {
                    component_separators = { left = '', right = '' },
                    section_separators = { left = '', right = '' },
                },
                sections = {
                    lualine_a = { 'mode' },
                    lualine_b = { 'filename' },
                    lualine_c = {},
                    lualine_x = { 'encoding', 'filetype' },
                    lualine_y = { 'progress' },
                    lualine_z = { 'location' },
                },
            }
        end,
    },

    -- Color schemes
    'jacoborus/tender.vim',
    'sainnhe/sonokai',
    'sam4llis/nvim-tundra',
    {
        'projekt0n/github-nvim-theme',
        name = 'github-theme',
    },

    -- LaTeX
    -- 'lervag/vimtex',
    -- let g:tex_flavor='latex',
    -- let g:vimtex_view_method='zathura',
    -- -- let g:vimtex_quickfix_mode=0
    -- set conceallevel=1
    -- let g:tex_conceal='abdmg',

    -- Alignment
    'godlygeek/tabular',

    -- Markdown
    'preservim/vim-markdown',

    -- Add/change/delete surrounding delimiter pairs with ease
    {
        'kylechui/nvim-surround',
        version = '*', -- Use for stability; omit to use `main` branch for the latest features
        event = 'VeryLazy',
        config = function()
            require('nvim-surround').setup {
                -- Configuration here, or leave empty to use defaults
            }
        end,
    },

    -- fzf
    {
        'ibhagwan/fzf-lua',
        config = function()
            vim.keymap.set(
                { 'n', 'v', 'o' },
                '<leader>o',
                '<cmd>FzfLua files<CR>'
            )
            vim.keymap.set(
                { 'n', 'v', 'o' },
                '<leader>p',
                '<cmd>FzfLua buffers<CR>'
            )
            vim.keymap.set(
                { 'n', 'v', 'o' },
                '<leader>g',
                '<cmd>FzfLua live_grep<CR>'
            )
        end,
    },

    -- Utils
    'nvim-lua/popup.nvim',
    'nvim-lua/plenary.nvim',
    'nvim-telescope/telescope.nvim',

    -- Tree
    {
        'nvim-neo-tree/neo-tree.nvim',
        branch = 'v3.x',
        dependencies = {
            'nvim-lua/plenary.nvim',
            'nvim-tree/nvim-web-devicons', -- not strictly required, but recommended
            'MunifTanjim/nui.nvim',
            '3rd/image.nvim', -- Optional image support in preview window: See `# Preview Mode` for more information
        },
        keys = {
            {
                '<leader>f',
                '<cmd>Neotree reveal<CR>',
                mode = '',
                desc = 'Toggle Neotree',
            },
            {
                '<leader>d',
                '<cmd>Neotree close<CR>',
                mode = '',
                desc = 'Close Neotree',
            },
            {
                '<leader>b',
                '<cmd>Neotree buffers reveal<CR>',
                mode = '',
                desc = 'Toggle Neotree for buffers',
            },
        },
    },
}
