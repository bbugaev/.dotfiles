return {
    {
        'nvim-treesitter/nvim-treesitter',
        build = ':TSUpdate',
        init = function(plugin)
            -- PERF: add nvim-treesitter queries to the rtp and it's custom query predicates early
            -- This is needed because a bunch of plugins no longer `require("nvim-treesitter")`, which
            -- no longer trigger the **nvim-treeitter** module to be loaded in time.
            -- Luckily, the only thins that those plugins need are the custom queries, which we make available
            -- during startup.
            require('lazy.core.loader').add_to_rtp(plugin)
            require('nvim-treesitter.query_predicates')
        end,
        cmd = { 'TSUpdateSync', 'TSUpdate', 'TSInstall' },
        keys = {
            { '<c-space>', desc = 'Increment selection' },
            { '<bs>', desc = 'Decrement selection', mode = 'x' },
        },
        opts = {
            highlight = { enable = true }, -- Use a colorscheme with TreeSitter support
            indent = { enable = true },
            ensure_installed = {
                'bash',
                'c',
                'cpp',
                'cmake',
                'comment',
                'css',
                'csv',
                'diff',
                'dot',
                'git_rebase',
                'gitignore',
                'glsl',
                'haskell',
                'html',
                'hurl',
                'java',
                'javascript',
                'jsdoc',
                'json',
                'jsonc',
                'latex',
                'llvm',
                'lua',
                'luadoc',
                'luap',
                'make',
                'markdown',
                'markdown_inline',
                'objdump',
                'python',
                'query',
                'regex',
                'rust',
                'sql',
                'toml',
                'vim',
                'vimdoc',
                'yaml',
            },
            incremental_selection = {
                enable = true,
                keymaps = {
                    init_selection = '<C-space>',
                    node_incremental = '<C-space>',
                    scope_incremental = false,
                    node_decremental = '<bs>',
                },
            },
        },
        config = function(_, opts)
            require('nvim-treesitter.configs').setup(opts)
        end,
    },
}
