vim.opt.encoding = 'utf-8' -- Set internal encoding of vim
vim.opt.fileencodings = { 'utf-8', 'cp1251' } -- List of automatically detected encodings
vim.opt.backspace = { 'indent', 'eol', 'start' } -- Make backspace work normally
vim.opt.foldenable = false -- Set syntax-based folding
vim.opt.foldmethod = 'syntax'
vim.opt.shortmess:append('I') -- Hide some mess
vim.opt.colorcolumn = '79' -- Red line in the 79th column
vim.opt.ruler = true -- Always show cursor position
vim.opt.showcmd = true -- Show uncompleted commands
vim.opt.number = true -- Enable line numbers
vim.opt.incsearch = true -- Search while typing
vim.opt.hlsearch = true -- Enable search result highlighting
vim.opt.ignorecase = true -- Ignore case in search
vim.opt.smartcase = true -- Override ignorecase when search includes uppercase
vim.opt.hidden = true -- Do not close the current buffer after loading a new one
vim.opt.autoindent = true -- Enable automatic indent
vim.cmd.colorscheme('sonokai') -- Set colorscheme
vim.opt.shiftwidth = 4 -- Default tab size
vim.opt.softtabstop = 4
vim.opt.tabstop = 4
vim.opt.smartindent = true -- Insert indents automatically
vim.opt.expandtab = true -- tab -> whitespaces...
vim.api.nvim_create_autocmd('FileType', {
    pattern = 'make',
    command = 'set noexpandtab',
}) -- ... except makefiles
vim.opt.list = true -- Highlight tabs and trailing whitespaces
vim.opt.listchars = { tab = '>·', trail = '·' }
vim.opt.wrap = false -- Disable line wrapping
vim.opt.linebreak = true -- Wrap lines between words
vim.opt.laststatus = 3 -- Views can only be fully collapsed with the global statusline
vim.opt.title = true -- Show buffer name in the terminal title
vim.opt.splitbelow = true -- hsplit below
vim.opt.splitright = true -- vsplit right
